# v1.0
# Add `~/bin` to the `$PATH`
export PATH="$HOME/bin:$PATH";
export KUBE_EDITOR="nano";

# Load the shell dotfiles, and then some:
# * ~/.path can be used to extend `$PATH`.
# * ~/.extra can be used for other settings you don’t want to commit.
for DOTFILE in ~/.{alias,bash_prompt,exports,functions,extra}; do
	[ -f "$DOTFILE" ] && . "$DOTFILE";
done;
unset DOTFILE;